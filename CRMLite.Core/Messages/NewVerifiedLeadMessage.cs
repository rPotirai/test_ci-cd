﻿using System;

namespace CRMLite.Core.Messages
{
    public class NewVerifiedLeadMessage
    {
        public Guid LeadID { get; set; }
    }
}
