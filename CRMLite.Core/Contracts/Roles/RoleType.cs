﻿namespace CRMLite.Core.Contracts.Roles
{
    public enum RoleType
    {
        User,
        Admin
    }
}
