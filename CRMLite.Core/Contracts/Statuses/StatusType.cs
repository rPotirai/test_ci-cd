﻿namespace CRMLite.Core.Contracts.Statuses
{
    public enum StatusType 
    {
        Regular,
        VIP,
        Blocked
    }
}
