﻿namespace CRMLite.RatesService
{
    public interface IRatesService
    {
        void CreateCurrencyExchangeRates();
        void CreateStockExchangeService();
    }
}
