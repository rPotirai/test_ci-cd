﻿namespace CRMLite.TransactionStoreDomain.Entities
{
    public class OperationType
    {
        public byte ID { get; set; }
        public string Type { get; set; }
    }
}
