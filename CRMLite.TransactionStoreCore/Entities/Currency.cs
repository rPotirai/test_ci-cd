﻿namespace CRMLite.TransactionStoreDomain.Entities
{
    public class Currency
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
    }
}