﻿namespace CRMLite.TransactionStoreDomain.Entities
{
    public class TFAModel
    {
        public string QRCodeBase64 { get; set; }
        public string ManualEntryKey { get; set; }
    }
}
