﻿namespace CRMLite.TransactionStoreAPI.RabbitMQ
{
    public class RabbitMQHostConfig
    {
        public string Host { get; set; }
        public string LocalHost { get; set; }
        public string Queue { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
