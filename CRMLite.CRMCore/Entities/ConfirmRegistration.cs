﻿namespace CRMLite.CRMCore.Entities
{
    public class ConfirmRegistration
    {
        public  bool IsConfirmed { get; set; }
        public  string Message { get; set; }
    }
}
