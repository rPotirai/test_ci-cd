﻿using System;

namespace CRMLite.CRMCore.Entities
{
    public class LeadAccessInfo
    {
        public Guid LeadID { get; set; }
        public string Token { get; set; }
    }
}
