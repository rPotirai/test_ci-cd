﻿namespace CRMLite.CRMCore.Entities
{
    public class SmtpOption
    {
        public string SenderMail { get; set; }
        public string SenderPassword { get; set; }
        public string SenderName { get; set; }
    }
}
